--- Required libraries.

-- Localised functions.
local transitionTo = transition.to
local cancelTransition = transition.cancel
local cancelTimer = timer.cancel
local performWithDelay = timer.performWithDelay

-- Localised values.

--- Class creation.
local library = {}

function library:init( params )

	-- Get the params, if any
	self._params = params or {}

	-- Loop through the params and set them
	for k, v in pairs( self._params ) do
		self[ k ] = v
	end

    -- Create the visual display group for the cursor
    self._visual = display.newGroup()

	-- Position us in the centre of the screen
	self._visual.x = display.contentCenterX
	self._visual.y = display.contentCenterY

    -- Set it to invisible for now
    self:hide()

    -- Table to hold our states
    self._states = {}

    -- Create a boring default state of a circle
    self._states[ "default" ] = display.newCircle( self._visual, 0, 0, 5 )

    -- Are we on a desktop?
    if self:onDesktop() then

        -- Hide the system mouse
        native.setProperty( "mouseCursorVisible", false )

	-- Or maybe mobile?
	elseif self:onMobile() then

		-- Disable us
		self:disable()

		-- And hide us
		self:disable()

		-- Show the system mouse
        native.setProperty( "mouseCursorVisible", true )

    end

    -- Add the mouse handler
	Runtime:addEventListener( "mouse", self )

    -- Set the state to default
    self:setState( "default" )

    -- Was a highlight colour passed in?
    if self._params.highlightColour then

        -- Then set the highlight colour
        self:setHighlightColour( self._params.highlightColour[ 1 ], self._params.highlightColour[ 2 ], self._params.highlightColour[ 3 ], self._params.highlightColour[ 4 ] or 1 )

    end

	-- Is the idle timer enabled?
	if self:isIdleTimerEnabled() then

		-- Then force us to sleep
		self:sleep( true )

	end

end

--- Hides the cursor.
function library:hide()
    self._visual.isVisible = false
end

--- Shows the cursor.
function library:show()
	if not self:onMobile() and self:isEnabled() then
    	self._visual.isVisible = true
	end
end

--- Checks if the cursor is visible.
-- @return True if it is, false otherwise.
function library:isVisible()
	return self._visual.isVisible and self._visual.alpha > 0.01
end

--- Enables the cursor.
function library:enable()
    if not self:onMobile() then
		self._disabled = false
	--	self:show()
	end
end

--- Disables the cursor.
function library:disable()
    self._disabled = true
	self:hide()
end

--- Checks if the cursor is enabled.
-- @return True if it is, false otherwise.
function library:isEnabled()
    return not self._disabled
end

--- Checks if we're on a mobile device.
-- @return True if we are, false otherwise.
function library:onMobile()
	return string.lower( system.getInfo( "platform" ) ) == "android" or string.lower( system.getInfo( "platform" ) ) == "ios"
end

--- Checks if we're on a desktop.
-- @return True if we are, false otherwise.
function library:onDesktop()
	return system.getInfo( "model" ) == "Desktop" or system.getInfo( "platform" ) == "macos" or system.getInfo( "platform" ) == "win32" or system.getInfo( "platform" ) == "linux"
end

--- Checks if we're on a console.
-- @return True if we are, false otherwise.
function library:onConsole()
	return system.getInfo( "platform" ) == "nx64"
end

--- Checks if we're in the simulator.
-- @return True if we are, false otherwise.
function library:inSimulator()
	return system.getInfo( "environment" ) == "simulator"
end

--- Sends us to the front.
function library:toFront()
    self._visual:toFront()
end

--- Update event handler. You must call this!
-- @param dt The current delta time.
function library:update( dt )

	-- Move us to the front
	self:toFront()

	-- Get the current position
	local position = self:getPosition( true )

	-- Do we have a position?
	if position and self:isEnabled() then

		-- Do we not have a last position or the last position is currently different to our previous position
        if not self._lastPosition or ( self._lastPosition.x ~= position.x or self._lastPosition.y ~= position.y ) then

			-- Wake us up
			self:wakeUp()

			-- Do we have a last position, i.e. we've definitely moved at least one pixel
			if self._lastPosition then

				-- Set the position
                self:setPosition( position.x, position.y )

				-- Set the state
                self:setState( "default" )

				-- Enable us
                self:enable()

				-- Wake us up ( before you go go! )
				self:wakeUp()

            end

        end

		-- Store out the position
        self._lastPosition = position

    end

	-- Are we on a desktop?
    if self:onDesktop() and not self:inSimulator() then

        -- Hide the system mouse
        native.setProperty( "mouseCursorVisible", false )

	end

end

--- Sets the position of the cursor.
-- @param x The x position.
-- @param y The y position.
-- @param silently If true then the cursor won't be woken up. Optional, defaults to false.
function library:setPosition( x, y, silently )

	-- Clamp the cursor along the X
	if x - self._visual.contentWidth * 0.5 + self:getState().x < display.safeScreenOriginX then
		x = self._visual.contentWidth * 0.5 - self:getState().x + display.safeScreenOriginX
	elseif x + self._visual.contentWidth * 0.5 + self:getState().x > display.actualContentWidth + display.safeScreenOriginX then
		x = display.actualContentWidth - self._visual.contentWidth * 0.5 - self:getState().x + display.safeScreenOriginX
	end

	-- Clamp the cursor along the Y
	if y - self._visual.contentHeight * 0.5 + self:getState().y < display.safeScreenOriginY then
		y = self._visual.contentHeight * 0.5 - self:getState().y + display.safeScreenOriginY
	elseif y + self._visual.contentHeight * 0.5 + self:getState().y > display.actualContentHeight + display.safeScreenOriginY then
		y = display.actualContentHeight - self._visual.contentHeight * 0.5 - self:getState().y - display.safeScreenOriginY
	end

    -- Are we enabled
    if self:isEnabled() then

        -- Set the position
        self._visual.x = x
        self._visual.y = y

		if ( not self._lastPosition or ( self._lastPosition.x ~= x or self._lastPosition.y ~= y ) ) and not silently then
			self:wakeUp()
		end

    end

	self.x = x
	self.y = y

	self._lastPosition = { x = x, y = y }

end

--- Gets the current position of the cursor.
-- @param asTable True if you want to get the position as a table, false if you want separate values. Optional, defaults to false.
-- @return The position, or the centre of the screen if not set.
function library:getPosition( asTable )
	if asTable then
		return { x = self.x or self._visual.x, y = self.y or self._visual.y }
	else
		return self.x or self._visual.x, self.y or self._visual.y
	end
end

--- Destroy a state.
-- @param name The name of the state.
function library:destroyState( name )

    -- Destroy the named state
    display.remove( self._states[ name ] )
    self._states[ name ] = nil

end

--- Create a state
-- @param params The params for the state.
function library:createState( params )

    -- Do we have any params?
    if params then

        -- Create a blank state
        local state

        -- Is this an image state?
        if params.image then

            -- Create the state from the image
            state = display.newImageRect( params.image, 16, 16 )

			-- Is there an offset?
			if params.offset then

				-- Then set it
				state.x = state.contentWidth * params.offset.x
				state.y = state.contentHeight * params.offset.y

			end

        end

        -- Do we have a state?
        if state then

            -- Does it have a fill colour?
            if params.fillColour then

                -- Set the colour
                state:setFillColor( params.fillColour[ 1 ], params.fillColour[ 2 ], params.fillColour[ 3 ], params.fillColour[ 4 ] or 1 )

            end

            -- And add the state
            self:addState( params.name, state )

        end

    end

end

--- Add a cursor state.
-- @param name The name of the state.
-- @param visual The visual object for the state.
function library:addState( name, visual )

    -- Was a name passed in and a visual?
    if name and visual then

        -- Destroy a same named state
        self:destroyState( name )

        -- Insert the new state into the visual
        self._visual:insert( visual )

        -- Store out the new state
        self._states[ name ] = visual

    end

end

--- Sets the current state of the cursor.
-- @param name The name of the state.
function library:setState( name )

	-- Do we have a passed in name and we're not currently in the state
	if name and self._currentState ~= name then

	    -- Was a name passed in that we have a state for?
	    if self._states[ name ] then

	        -- Loop through all the registered states
	        for k, v in pairs( self._states ) do

	            -- Mark it as visible only if it's the desired one
	            v.isVisible = k == name

				-- Put its alpha back to one if it's the desired one
				--v.alpha = k == name and 1 or 0.01

	        end

	        -- Store out the name of the state
	        self._currentState = name

	    end

		-- If it should be hidden because it's idle then hide it
		if self:isIdle() then
			self:sleep( true )
		end

		-- If it should be highlighted then highlight it
		if self:isHighlighted() then
			self:highlight()
		end

	end

end

--- Gets a state.
-- @param name The name of the state.
function library:getState( name )
    return name and self._states[ name ] or self:getCurrentState() and self:getState( self:getCurrentState() )
end

--- Sets the cursor scale.
-- @param scale The scale to set.
function library:setScale( scale )
    self._visual.xScale = scale
    self._visual.yScale = scale
end

--- Gets the name of the state.
-- @return The state.
function library:getCurrentState()
    return self._currentState
end

-- Highlights the cursor
function library:highlight()

    -- Mark us as highlighted
    self._isHighlighted = true

	self:wakeUp()

    -- Do we have a current state and a highlight colour
    if self:getState() and self:getHighlightColour() then
    --    local r, g, b, a = self:getHighlightColour()
        self:getState():setFillColor( self:getHighlightColour() )
    end

end

-- Unhighlights the cursor
function library:unhighlight()

    self._isHighlighted = false

    if self:getState() then
        self:getState():setFillColor( 1, 1, 1, 1 )
    end

end

--- Checks if the cursor is currently highlighted.
-- @return True if it is, false otherwise.
function library:isHighlighted()
    return self._isHighlighted
end

--- Sets the highlight colour.
-- @param r The R value of the colour.
-- @param g The G value of the colour.
-- @param b The B value of the colour.
-- @param a The A value of the colour.
function library:setHighlightColour( r, g, b, a )
    self._highlightColour = { r, g, b, a }
end

--- Gets the current highlight colour.
-- @return The R, G, B, and A values of the colour.
function library:getHighlightColour()
    if self._highlightColour then
        return self._highlightColour[ 1 ] or 1, self._highlightColour[ 2 ] or 1, self._highlightColour[ 3 ] or 1, self._highlightColour[ 4 ] or 1
    end
end


--- Called automatically when the cursor is idle.
-- @param force True if you wish to force the cursor to be idle instantly.
function library:sleep( force )
	
	-- Do we have a state?
	if self:getState() then
		
		-- And we're not already idle?
		if not self:isIdle() then
			
			-- Cancel any existing fade transition
			if self._fadeTransition then
				cancelTransition( self._fadeTransition )
		    	self._fadeTransition = nil
			end

			-- And fade us out
		    self._fadeTransition = transitionTo( self._visual, { time = self.idleFadeOutTime or 250, alpha = 0.01 } )

		end
--force  = true
		-- Were we forced?
		if force then

			-- Then hide us straight away!
			self._visual.alpha = 0.01

		end

		-- Flag us as idle
		self._isIdle = true

	end



end

--- Wake us up from idle.
function library:wakeUp()	
	
	-- Do we have a state?
	if self:getState() then
	
		-- Show it!
		self._visual.alpha = 1
	
	end
	
	if self:isIdle() and self:isEnabled() then		
	
		-- Cancel any existing fade transition
		if self._fadeTransition then
			cancelTransition( self._fadeTransition )
			self._fadeTransition = nil
		end
		
		-- Cancel any existing timer
		if self._idleTimer then
			cancelTimer( self._idleTimer )
			self._idleTimer = nil
		end
		
		-- Mark us as not idle
		self._isIdle = false

		-- Restart the idle timer
		self:startIdleTimer()

	end

end

--- Check if the cursor is currently idle.
-- @return True if it is, false otherwise.
function library:isIdle()
	return self._isIdle
end

--- Check if the cursor is currently awake.
-- @return True if it is, false otherwise.
function library:isAwake()
	return not self:isIdle()
end

--- Enables the idle timer.
-- @param time The delay for waiting for movement. Defaults to whatever it already is or the default value of 3000 milliseconds.
-- @param fade The fade out time. Defaults to whatever it already is or the default value of 250 milliseconds.
function library:enableIdleTimer( time, fade )

	-- Cancel any existing timer
	if self._idleTimer then
		cancelTimer( self._idleTimer )
		self._idleTimer = nil
	end

	-- Nil out the last position
	self._lastPosition = nil

	-- Enable the timer
	self.hideWhenIdle = true

	-- Set the idle delay
	self.idleTime = time or self.idleTime

	-- And the fade out time
	self.idleFadeOutTime = fade or self.idleFadeOutTime

end

--- Disables the idle timer.
function library:disableIdleTimer()
	
	-- Cancel any existing timer
	if self._idleTimer then
		cancelTimer( self._idleTimer )
		self._idleTimer = nil
	end

	-- Disable the timer
	self.hideWhenIdle = false

end

--- Check if the idle timer is enabled.
-- @return True if it is, false otherwise.
function library:isIdleTimerEnabled()
	return self.hideWhenIdle
end

--- Start the idle timer.
function library:startIdleTimer()

	-- Cancel any existing timer
	if self._idleTimer then
		cancelTimer( self._idleTimer )
		self._idleTimer = nil
	end
	
	-- Is the idle timer enabled?
	if self:isIdleTimerEnabled() then
		
		-- Start the new timer
		self._idleTimer = performWithDelay( self.idleTime or 3000, function() self:sleep() end, 1 )

	end

end

--- Destroys the time library.
function library:destroy()

    -- Nil out the states
    self._states = nil

    -- Destroy the current cursor
    display.remove( self._visual )
    self._visual = nil

end

-- If we don't have a global Scrappy object i.e. this is the first Scrappy plugin to be included
if not Scrappy then

	-- Create one
	Scrappy = {}

end

-- If we don't have a Scrappy Cursor library
if not Scrappy.Cursor then

	-- Then store the library out
	Scrappy.Cursor = library

end

-- Return the new library
return library
